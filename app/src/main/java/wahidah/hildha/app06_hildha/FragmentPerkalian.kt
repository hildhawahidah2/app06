package wahidah.hildha.app06_hildha

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.frag_data_prodi.view.*
import kotlinx.android.synthetic.main.frag_perkalian.*
import kotlinx.android.synthetic.main.frag_perkalian.view.*
import java.text.DecimalFormat

class FragmentPerkalian: Fragment(), View.OnClickListener{

    lateinit var thisParent: MainActivity
    lateinit var v: View

    var x : Double = 0.0
    var y : Double = 0.0
    var hasil : Double = 0.0

    override fun onClick(v: View?) {
        x = edx.text.toString().toDouble()
        y = edy.text.toString().toDouble()
        hasil = x*y
        txhasilkali.text = DecimalFormat("#.##").format(hasil)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        //db = thisParent.getDBObject()
        v = inflater.inflate(R.layout.frag_perkalian, container, false)
        v.btn.setOnClickListener(this)

        return v

                //(savedInstanceState: Bundle?) {
        //super.onCreate(savedInstanceState)
        //setContentView(R.layout.frag_perkalian)
        //btn.setOnClickListener(this)

        //var paket : Bundle? = intent.extras
        // edx.setText(paket?.getString("x"))
    }


}